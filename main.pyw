from genericpath import isfile
from msilib.schema import File
import PySimpleGUI as sg
from PIL import ImageGrab, Image
import subprocess
import os
from tkinter import filedialog


class FileAction:
    import sys
    import os

    def ConvertFileLink(str):
        data = FileAction.os
        base = data.path.dirname(FileAction.sys.argv[0])
        name = data.path.normpath(data.path.join(base, str))
        return name


class OCR:
    import os
    from PIL import Image
    import pyocr

    power = False
    tool = object
    tesseract_path = FileAction.ConvertFileLink("Tesseract-OCR\\tesseract.exe")

    def onload():
        OCR.pyocr.tesseract.TESSERACT_CMD = OCR.tesseract_path
        tools = OCR.pyocr.get_available_tools()
        OCR.tool = tools[0]
        OCR.power = True

    def read(FilePath, Accuracy):
        # 画像ファイルパス, 読み取り精度(1～10)
        if OCR.power == False:
            OCR.onload()

        if Accuracy < 1:
            Accuracy = 1
        if Accuracy > 10:
            Accuracy = 10

        img = OCR.Image.open(FilePath)

        builder = OCR.pyocr.builders.TextBuilder(tesseract_layout=Accuracy)
        text = OCR.tool.image_to_string(img, lang="jpn", builder=builder)
        return text


class ImageFiles:
    ImageFilePath = ""

    def SetImageFilePath(Path):
        ImageFiles.ImageFilePath = Path

    def GetImageFilePath():
        return ImageFiles.ImageFilePath


class Clipboard:
    def read(window):
        # クリップボードの画像読み込み
        img = ImageGrab.grabclipboard()
        if img != None:
            img.save("ClipboardImage.png")
            Path = FileAction.ConvertFileLink("ClipboardImage.png")
            Flg = os.path.isfile(Path)
            if Flg == True:
                # ファイルが正常に作成された場合
                ImageFiles.SetImageFilePath(Path)
                OCRWrite.write(window)
        else:
            sg.popup("クリップボードに画像がコピーされていません。", title="エラー")


class OCRWrite:
    def write(window):
        Path = ImageFiles.GetImageFilePath()
        NowDescription = DescriptionSettings.NowDescription
        Flg = os.path.isfile(Path)
        if Flg == True and NowDescription != 2:
            text = OCR.read(Path, NowDescription)
            window["result"].update(text)
        elif NowDescription == 2:
            sg.popup("未実装の読み取り精度が選択されました。\n読み取り精度の設定を確認してください。", title="エラー")
        else:
            sg.popup(
                f"画像ファイルが存在しないか選択されていません。\n\n選択された画像ファイルパス\n{Path}", title="エラー")


class FileSelect:
    dir = 'C:\\'

    def OpenFileSelectWindow(window, values):
        typ = [("Image file", ".jpg .png"),
               ('jpgファイル', '*.jpg'), ("pngファイル", "*.png")]
        fle = filedialog.askopenfilename(
            filetypes=typ, initialdir=FileSelect.dir)

        ImageFiles.SetImageFilePath(fle)

        if fle != "":
            OCRWrite.write(window)
        else:
            window["result"].update("")

        Flg1 = values["SaveDirectory"]
        Flg2 = os.path.isfile(fle)
        if Flg1 == True and Flg2 == True:
            FileSelect.dir = os.path.dirname(fle)


AccuracyDescription = [
    "自動ページ分割とOSD。",
    "自動ページ分割、ただしOSD、またはOCRはなし。(未実装)",
    "完全自動ページ分割、ただしOSDなし。(初期設定)",
    "サイズの異なるテキストが1列に並んでいると仮定します。",
    "縦書きの一様なテキストブロックを想定しています。",
    "一様なテキストブロックを想定しています。",
    "画像を1つのテキスト行として扱う。",
    "画像を1つの単語として扱う。",
    "画像を円内の単一単語として扱う。",
    "画像を1つの文字として扱う。",
    "疎なテキスト。できるだけ多くのテキストを順不同に探します。",
    "OSDでテキストを疎にする。",
    "生の行。画像を1つのテキスト行として扱います。"
]


class DescriptionSettings:
    DefaultDescription = 6
    NowDescription = DefaultDescription


ButtonSize = 39

# 画面構成
layout = [
    [sg.Text("ファイル選択・読み取り精度設定", font=('Arial', 20))],
    [sg.HorizontalSeparator()],
    [sg.Button("画像ファイルを選択する", size=(ButtonSize, 1), font=(
        'Arial', 15), key="SelectImage"), sg.Button("クリップボードの画像を読み込む", size=(ButtonSize, 1), font=('Arial', 15), key="ClipboardImage")],
    [sg.Button("画像ファイルパスを確認する", size=(ButtonSize, 1), font=('Arial', 15), key="CheckImageFilePath"),
     sg.Button("画像ファイルを開く", size=(ButtonSize, 1), font=('Arial', 15), key="OpenImageFile")],
    [sg.Slider(range=(1.0, 13.0), default_value=DescriptionSettings.DefaultDescription, orientation='h', size=(ButtonSize - 5.5, 20), key="Accuracy", enable_events=True, font=('Arial', 15)), sg.Checkbox("読み取りフォルダを保存する\t\n※ソフト起動中のみ保存\t",  font=(
        'Arial', 15), key="SaveDirectory", default=True, size=(ButtonSize - 2, 1))],
    [sg.Text("説明：", font=('Arial', 15)), sg.Text(
        AccuracyDescription[DescriptionSettings.DefaultDescription - 1], key="AccuracyDescription", font=('Arial', 15))],
    [sg.Text("\n\n")],
    [sg.Text("読み取り結果", font=('Arial', 20)), sg.Text("\t"), sg.Button(
        "🔄 再読み込み", font=('Arial', 15), key="reload", size=(15, 1))],
    [sg.HorizontalSeparator()],
    [sg.Multiline(size=(200, 100), font=('Arial', 20), key="result")]
]

# ウィンドウの生成
window = sg.Window("OCR", layout, size=(900, 600))


def ChangeDescription(window, values):
    range = int(values["Accuracy"])
    DescriptionSettings.NowDescription = range
    text = AccuracyDescription[range - 1]
    window["AccuracyDescription"].update(text)


print(OCR.tesseract_path)
OCREngine = os.path.isfile(OCR.tesseract_path)

if OCREngine == True:
    # イベントループ
    while True:
        event, values = window.read()

        if event == sg.WIN_CLOSED:
            # ウィンドウが閉じられた時に処理を抜ける

            # クリップボードから取得した画像ファイルがあるならば削除する
            CashFile = os.path.isfile("ClipboardImage.png")
            if CashFile == True:
                os.remove("ClipboardImage.png")

            break
        elif event == "SelectImage":
            FileSelect.OpenFileSelectWindow(window, values)
        elif event == "ClipboardImage":
            # クリップボードの画像を読み取りOCR処理をし出力する
            Clipboard.read(window)
        elif event == "CheckImageFilePath":
            Path = ImageFiles.GetImageFilePath()
            if Path == "":
                Path = "画像ファイルが選択されていません。"
            sg.popup(Path, title="画像ファイルパスを確認する")
        elif event == "OpenImageFile":
            # 画像ファイルを規定のアプリケーションで開く
            Path = ImageFiles.GetImageFilePath()
            Flg = os.path.isfile(Path)
            if Flg == True:
                subprocess.Popen(["start", Path], shell=True)
            else:
                sg.popup("画像ファイルが存在しません。", title="エラー")
        elif event == "Accuracy":
            # 読み取り精度の説明表示切替
            ChangeDescription(window, values)
        elif event == "reload":
            Path = ImageFiles.GetImageFilePath()
            Flg = os.path.isfile(Path)
            if Flg == True:
                OCRWrite.write(window)
            else:
                sg.popup("画像ファイルが存在しません。", title="エラー")
else:
    # OCRエンジンが見つからなかった時の動作
    sg.popup(
        f"OCRエンジンがありません。\nプログラムを終了します。\n\nOCRファイルパス\n{OCR.tesseract_path}", title="エラー")


window.close()
